
const React = require('react')
const antd = require('antd')

class WindSpeed extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {  
    var polygon = React.createElement(
      'polygon',
      {
        points: '-5,10 0,5 5,10 0,-15',
        fill: 'black',
        transform: 'rotate(' + this.props.degrees + ')',
      },
      null
    );
    
    var svg = React.createElement(
      'svg',
      {
        key: 'svg',
        viewBox: '-25 -25 50 50',
        width: 50,
        height: 50,
      },
      polygon
    );
    
    var text = React.createElement(
      'span',
      {
        key: 'text'
      },
      Math.round(this.props.speed * 3.6) + ' km/h'
    );
  
    return React.createElement(
      antd.Space,
      {
        align: 'center'
      },
      [svg, text]
    );
  }
  
}

module.exports = WindSpeed;



