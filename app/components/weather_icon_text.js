
const React = require('react')
const antd = require('antd')

class WeatherIconText extends React.Component {

  constructor(props) {
    super(props);
  }

  render() { 
    var imgSize = this.props.imgSize ? this.props.imgSize : 50;
   
    var icon = React.createElement(
      'img',
      {
        key: 'icon',
        src: 'http://openweathermap.org/img/wn/' + this.props.icon + '@2x.png',
        width: imgSize,
        height: imgSize,
      },
      null
    );
    
    var text = React.createElement(
      'span',
      {
        key: 'text'
      },
      this.props.text
    );
  
    return React.createElement(
      antd.Space,
      {
        align: 'baseline'
      },
      [icon, text]
    );
  }
  
}

module.exports = WeatherIconText;



