
function getCityDisplayName(city) {
  var displayName = city.name;
  if(city.state) {
    displayName = displayName + ' - ' + city.state;
  }
  displayName = displayName + ' (' + city.country + ')';
  
  return displayName;
}

function cutAngleInMinSec(angle) {
  var deg = Math.round(angle);
  var rem = (angle % 1) * 60;
  var min = Math.round(rem);
  var sec = Math.round((rem % 1) * 60);
  
  return {deg: deg, min: min, sec: sec};
}

function angleToMinSecString(angle) {
  var dms = cutAngleInMinSec(angle);
  return dms.deg + '° ' + dms.min + '\' ' + dms.sec + '\'\'';
}

function getCityLatLonAsString(city) {
  return angleToMinSecString(city.coord.lat) + '; ' + 
    angleToMinSecString(city.coord.lon);
}

module.exports = {
  getCityDisplayName: getCityDisplayName,
  getCityLatLonAsString: getCityLatLonAsString,
}



