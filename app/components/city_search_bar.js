
const fs = require('fs')
const React = require('react')
const antd = require('antd')

const {getCityDisplayName} = require('../city_utils.js')

class CitySearchBar extends React.Component {

  constructor(props) {
    super(props);  
    
    this.state = {candidates: []};
    
    this.citiesDatabase = [];
    
    this.loadCityList();
  }
  
  // Start the async load of cities database
  loadCityList() {
    fs.readFile(
      './resources/city.list.json', 
      'utf8', 
      this.onCityListLoaded.bind(this)
    );
  }
  
  // Callback after cities database load completes
  onCityListLoaded(err, data) {
    if(err) {
      return console.log(err);
    }
    
    this.citiesDatabase = JSON.parse(data);
  }
  
  // Callback for user search
  onSearch(value) {
    if(this.citiesDatabase.length > 0 && value.length > 3) {
      // filter database 
      var lvalue = value.toLowerCase();
      var filtered = this.citiesDatabase.filter(function (city) {
        return city.name.toLowerCase().includes(lvalue);
      });
      this.setState({candidates: filtered});
    } else {
      this.setState({candidates: []});
    }
  }
  
  // Callback on city selection 
  onSelect(value, option) {    
    // search back the city in the database 
    var city = this.citiesDatabase.find(function (city) {
      return city.id == option.key;
    });
    
    // upper callback 
    if(typeof this.props.onSelect === "function") {
      this.props.onSelect(city);
    }
  }

  render() {
    // create options
    var options = this.state.candidates.map(function(city) {    
      return React.createElement(
        antd.AutoComplete.Option,
        {
          key: city.id, 
          value: getCityDisplayName(city),
        },
        null
      );
    });
    
    // create and return autocomplete component  
    return React.createElement(
      antd.AutoComplete,
      {
        onSearch: this.onSearch.bind(this),
        onSelect: this.onSelect.bind(this),
        placeholder: 'City name',
        style: {width: '100%'}
      }, 
      options
    );  
  }

};



module.exports = CitySearchBar;



